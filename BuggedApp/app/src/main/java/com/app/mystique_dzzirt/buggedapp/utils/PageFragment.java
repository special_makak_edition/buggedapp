/**
 * Created by Admin on 22.12.2016.
 */
package com.app.mystique_dzzirt.buggedapp.utils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ImageView;

import com.app.mystique_dzzirt.buggedapp.AboutAuthorsActivity;
import com.app.mystique_dzzirt.buggedapp.AboutAuthorsActivity_;
import com.app.mystique_dzzirt.buggedapp.MainActivity_;
import com.app.mystique_dzzirt.buggedapp.R;

public class PageFragment extends Fragment
{
    private int pageNumber;

    public static PageFragment newInstance(int page) {
        PageFragment fragment = new PageFragment();
        Bundle args=new Bundle();
        args.putInt("num", page);
        fragment.setArguments(args);
        return fragment;
    }

    public PageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = getArguments() != null ? getArguments().getInt("num") : 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View result=inflater.inflate(R.layout.fragment_page, container, false);
        TextView pageHeader=(TextView)result.findViewById(R.id.displayText);

        String descriptionTexts[] = {
                "Окно логинации для создания аккаунтов. Аккаунт admin/1234 для создания записей.",
                "Выбор записи на стрижку.",
                "Возможность добавлять записи in list.",
                "Возможность добавлять новый персонал и виды услуг."
        };

        pageHeader.setText(descriptionTexts[pageNumber]);

        Button aboutAuthorsButton=(Button)result.findViewById(R.id.about_authors);
        aboutAuthorsButton.setVisibility(pageNumber + 1 < 4 ? View.GONE : View.VISIBLE);
        aboutAuthorsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AboutAuthorsActivity_.intent(getActivity()).start();
            }
        });

        Button skipButton=(Button)result.findViewById(R.id.skipButton);
        skipButton.setVisibility(pageNumber + 1 < 4  && pageNumber > 0 ? View.VISIBLE : View.GONE);
        if (pageNumber + 1 != 3) {
            skipButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainActivity_.intent(getActivity()).start();
                }
            });
        }

        Button letsWorkButton=(Button)result.findViewById(R.id.letsWorkButton);
        letsWorkButton.setVisibility(pageNumber + 1 < 4 ? View.GONE : View.VISIBLE);
        letsWorkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity_.intent(getActivity()).start();
            }
        });

        int imagesIds[] = {R.drawable.gallery_screen_1, R.drawable.gallery_screen_2, R.drawable.gallery_screen_3, R.drawable.gallery_screen_4};

        ImageView mainImageView = (ImageView)result.findViewById(R.id.mainImage);
        mainImageView.setImageResource(imagesIds[pageNumber]);

        return result;
    }

}