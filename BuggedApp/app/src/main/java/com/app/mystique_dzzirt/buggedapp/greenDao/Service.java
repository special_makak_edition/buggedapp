package com.app.mystique_dzzirt.buggedapp.greenDao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by Dzzirt on 20.12.2016.
 */

@Entity
public class Service {

    @Id
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Date duration;

    @NotNull
    private int price;

    @Generated(hash = 1056019775)
    public Service(Long id, @NotNull String name, @NotNull Date duration,
            int price) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.price = price;
    }

    @Generated(hash = 552382128)
    public Service() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDuration() {
        return this.duration;
    }

    public void setDuration(Date duration) {
        this.duration = duration;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
}
