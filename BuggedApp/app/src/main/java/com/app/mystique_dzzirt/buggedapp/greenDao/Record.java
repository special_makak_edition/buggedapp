package com.app.mystique_dzzirt.buggedapp.greenDao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.NotNull;

import java.util.Date;

/**
 * Created by Dzzirt on 20.12.2016.
 */

@Entity
public class Record {

    @Id
    private Long id;

    @NotNull
    private String date;

    private long availableServicesId;
    
    @ToOne(joinProperty = "availableServicesId")
    private AvailableServices availableServices;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 765166123)
    private transient RecordDao myDao;

    @Generated(hash = 1896744776)
    public Record(Long id, @NotNull String date, long availableServicesId) {
        this.id = id;
        this.date = date;
        this.availableServicesId = availableServicesId;
    }

    @Generated(hash = 477726293)
    public Record() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getAvailableServicesId() {
        return this.availableServicesId;
    }

    public void setAvailableServicesId(long availableServicesId) {
        this.availableServicesId = availableServicesId;
    }

    @Generated(hash = 1499410975)
    private transient Long availableServices__resolvedKey;

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 791412102)
    public AvailableServices getAvailableServices() {
        long __key = this.availableServicesId;
        if (availableServices__resolvedKey == null
                || !availableServices__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            AvailableServicesDao targetDao = daoSession.getAvailableServicesDao();
            AvailableServices availableServicesNew = targetDao.load(__key);
            synchronized (this) {
                availableServices = availableServicesNew;
                availableServices__resolvedKey = __key;
            }
        }
        return availableServices;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1967245829)
    public void setAvailableServices(@NotNull AvailableServices availableServices) {
        if (availableServices == null) {
            throw new DaoException(
                    "To-one property 'availableServicesId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.availableServices = availableServices;
            availableServicesId = availableServices.getId();
            availableServices__resolvedKey = availableServicesId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1505145191)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getRecordDao() : null;
    }

}
