package com.app.mystique_dzzirt.buggedapp.utils;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app.mystique_dzzirt.buggedapp.R;

import java.util.Calendar;

/**
 * Created by Dzzirt on 22.12.2016.
 */

public class DatePicker extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private int m_textViewId;

    public DatePicker() {

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // определяем текущую дату
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // создаем DatePickerDialog и возвращаем его
        return new DatePickerDialog(getActivity(), this,
                year, month, day);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int year,
                          int month, int day) {
        m_textViewId = getArguments().getInt("id", 0);
        EditText editText = (EditText) getActivity().findViewById(m_textViewId);
        editText.setText(day + "." + month + "." + year);
    }
}
