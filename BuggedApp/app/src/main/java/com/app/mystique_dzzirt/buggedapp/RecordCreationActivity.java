package com.app.mystique_dzzirt.buggedapp;

import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.mystique_dzzirt.buggedapp.greenDao.AvailableServices;
import com.app.mystique_dzzirt.buggedapp.greenDao.AvailableServicesDao;
import com.app.mystique_dzzirt.buggedapp.greenDao.DaoSession;
import com.app.mystique_dzzirt.buggedapp.greenDao.Hairdresser;
import com.app.mystique_dzzirt.buggedapp.greenDao.Record;
import com.app.mystique_dzzirt.buggedapp.greenDao.RecordDao;
import com.app.mystique_dzzirt.buggedapp.greenDao.Service;
import com.app.mystique_dzzirt.buggedapp.greenDao.ServiceDao;
import com.app.mystique_dzzirt.buggedapp.utils.DatePicker;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

/**
 * Created by Dzzirt on 21.12.2016.
 */
@EActivity(R.layout.record_creation_activity)
public class RecordCreationActivity extends AppCompatActivity {

    @ViewById(R.id.hairdressers_selection)
    public Spinner m_hairdressersSelection;

    @ViewById(R.id.service_selection)
    public Spinner m_serviceSelection;

    @ViewById(R.id.record_creation_toolbar)
    public Toolbar m_toolbar;

    @ViewById(R.id.date_selection)
    public EditText m_date;

    @ViewById(R.id.price_selection)
    public TextView m_price;

    @ViewById(R.id.create_hairdresser)
    public FloatingActionButton m_createHairdresserBtn;

    @ViewById(R.id.multiple_actions)
    public FloatingActionsMenu m_actionMenu;

    @ViewById(R.id.delete_record_btn)
    public Button m_deleteButton;

    //TODO создание услуги
    //TODO Запилить галерею
    //TODO сохранение записи
    @ViewById(R.id.create_service)
    public FloatingActionButton m_createServiceBtn;

    @Extra(value = "recordId")
    public long recordId = -1;

    private List<AvailableServices> m_availableServices;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_changes_menu, menu);
        return true;
    }

    @AfterViews
    public void init() {
        setSupportActionBar(m_toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        SharedPreferences admin_prefs = getSharedPreferences("admin_prefs", MODE_PRIVATE);
        boolean is_admin = admin_prefs.getBoolean("is_admin", false);
        m_actionMenu.setVisibility(is_admin ? View.VISIBLE : View.GONE);

        m_createHairdresserBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HairdresserCreationActivity_.intent(view.getContext()).start();
            }
        });

        m_createServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServiceCreationActivity_.intent(view.getContext()).start();
            }
        });
        initTextFields();


        m_deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DaoSession daoSession = ((App) getApplication()).getDaoSession();
                daoSession.getRecordDao().deleteByKey(recordId);
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        initTextFields();
    }

    private void initTextFields() {
        m_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dateDialog = new DatePicker();
                Bundle bundle = new Bundle();
                bundle.putInt("id", R.id.date_selection);
                dateDialog.setArguments(bundle);
                dateDialog.show(getSupportFragmentManager(), "datePicker");
            }
        });


        final DaoSession daoSession = ((App) getApplication()).getDaoSession();
        final List<Hairdresser> list = daoSession.getHairdresserDao().queryBuilder().build().list();
        String[] hairdressersNames = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            hairdressersNames[i] = list.get(i).getName();
        }

        ArrayAdapter<String> adapter = getSpinnerAdapter(hairdressersNames);
        m_hairdressersSelection.setAdapter(adapter);

        m_hairdressersSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                QueryBuilder<AvailableServices> query = daoSession.getAvailableServicesDao().queryBuilder()
                        .where(AvailableServicesDao.Properties.HairdresserId.eq(list.get(i).getId()));
                m_availableServices = query.build().list();
                servicesSelectionInit();
                Log.d("", "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        m_serviceSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                AvailableServices availableServices = m_availableServices.get(i);
                Hairdresser hairdresser = availableServices.getHairdresser();
                Service service = availableServices.getService();
                m_price.setText(String.valueOf(hairdresser.getAllowance() + service.getPrice()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void servicesSelectionInit() {
        String[] servicesNames = new String[m_availableServices.size()];
        for (int j = 0; j < m_availableServices.size(); j++) {
            AvailableServices availableServices = m_availableServices.get(j);
            String serviceName = availableServices.getService().getName();
            servicesNames[j] = serviceName;
        }
        ArrayAdapter<String> adapter = getSpinnerAdapter(servicesNames);
        m_serviceSelection.setAdapter(adapter);
    }

    private ArrayAdapter<String> getSpinnerAdapter(String[] data) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_save : {
                final DaoSession daoSession = ((App) getApplication()).getDaoSession();
                AvailableServicesDao availableServicesDao = daoSession.getAvailableServicesDao();
                RecordDao recordDao = daoSession.getRecordDao();
                Record record;
                AvailableServices availableServices = null;
                long selectedHairdresserId = m_hairdressersSelection.getSelectedItemId();
                Service service = daoSession.getServiceDao().queryBuilder()
                        .where(ServiceDao.Properties.Name.eq(m_serviceSelection.getSelectedItem()))
                        .build().unique();
                /*if (availableServicesDao.queryBuilder()
                        .where(AvailableServicesDao.Properties.HairdresserId.eq(selectedHairdresserId + 1),
                                AvailableServicesDao.Properties.ServiceId.eq(seletedServiceId + 1)).build().list().size() == 0) {
                    availableServices = new AvailableServices();
                    availableServices.setHairdresserId(selectedHairdresserId + 1);
                    availableServices.setServiceId(seletedServiceId + 1);
                }*/
                if (recordId == -1) {
                    record = new Record();

                } else {
                    record = daoSession.getRecordDao().queryBuilder()
                            .where(RecordDao.Properties.Id.eq(recordId)).build().unique();
                }
                record.setDate(m_date.getText().toString());
                List<AvailableServices> availableServices1 = availableServicesDao.loadAll();
                List<Service> services = daoSession.getServiceDao().loadAll();
                AvailableServices unique = availableServicesDao.queryBuilder()
                        .where(AvailableServicesDao.Properties.HairdresserId.eq(selectedHairdresserId + 1))
                        .where(AvailableServicesDao.Properties.ServiceId.eq(service.getId()))
                        .build().unique();
                record.setAvailableServices(unique);

                if (!recordDao.hasKey(record)) {
                    recordDao.insert(record);
                }

                break;
            }
        }
        finish();
        return true;
    }
}
