package com.app.mystique_dzzirt.buggedapp.greenDao;

import com.app.mystique_dzzirt.buggedapp.utils.Gender;

import org.greenrobot.greendao.converter.PropertyConverter;

/**
 * Created by Dzzirt on 20.12.2016.
 */

public class GenderConverter implements PropertyConverter<Gender, String> {
    @Override
    public Gender convertToEntityProperty(String databaseValue) {
        return Gender.valueOf(databaseValue);
    }

    @Override
    public String convertToDatabaseValue(Gender entityProperty) {
        return entityProperty.name();
    }
}
