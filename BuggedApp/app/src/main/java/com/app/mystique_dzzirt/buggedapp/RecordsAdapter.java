package com.app.mystique_dzzirt.buggedapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.mystique_dzzirt.buggedapp.greenDao.AvailableServices;
import com.app.mystique_dzzirt.buggedapp.greenDao.Hairdresser;
import com.app.mystique_dzzirt.buggedapp.greenDao.Record;
import com.app.mystique_dzzirt.buggedapp.greenDao.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Dzzirt on 20.12.2016.
 */

public class RecordsAdapter extends RecyclerView.Adapter<RecordsAdapter.RecordsViewHolder> {

    private List<Record> m_records;

    private RecyclerView m_recyclerView;

    public RecordsAdapter(RecyclerView recyclerView, List<Record> records) {
        m_records = records;
        m_recyclerView = recyclerView;
    }

    public void updateData(List<Record> data) {
        m_records = data;
        notifyDataSetChanged();
    }

    public class RecordsViewHolder extends RecyclerView.ViewHolder {

        public TextView date;
        public TextView hairdresser;
        public TextView service;

        public RecordsViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            hairdresser = (TextView) itemView.findViewById(R.id.hairdresser);
            service = (TextView) itemView.findViewById(R.id.service);
        }
    }

    public RecordsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.record_tile, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int childLayoutPosition = m_recyclerView.getChildLayoutPosition(view);
                //Bug
                if (childLayoutPosition > 29) {
                    throw new RuntimeException();
                }
                Record record = m_records.get(childLayoutPosition);
                RecordCreationActivity_.intent(view.getContext()).recordId(record.getId()).start();
            }
        });
        return new RecordsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecordsViewHolder holder, int position) {
        //Bug
        emulateFreezes();

        Record record = m_records.get(position);
        AvailableServices availableServices = record.getAvailableServices();
        Hairdresser hairdresser = availableServices.getHairdresser();
        Service service = availableServices.getService();

        holder.date.setText(record.getDate().toString());
        holder.hairdresser.setText(hairdresser.getName());
        holder.service.setText(service.getName());
    }

    private void emulateFreezes() {
        //Bug
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return m_records.size();
    }
}
