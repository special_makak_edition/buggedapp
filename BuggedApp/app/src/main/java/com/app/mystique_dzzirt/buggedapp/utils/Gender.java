package com.app.mystique_dzzirt.buggedapp.utils;

/**
 * Created by Dzzirt on 20.12.2016.
 */

public enum Gender {
    MALE,
    FEMALE
}
