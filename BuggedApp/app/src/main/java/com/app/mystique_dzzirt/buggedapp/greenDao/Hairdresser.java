package com.app.mystique_dzzirt.buggedapp.greenDao;

import com.app.mystique_dzzirt.buggedapp.utils.Gender;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

/**
 * Created by Dzzirt on 20.12.2016.
 */
@Entity
public class Hairdresser {

    @Id
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Integer experience;

    @NotNull
    private String education;

    @NotNull
    private int age;

    @Convert(converter = GenderConverter.class, columnType = String.class)
    private Gender gender;

    @NotNull
    private int allowance;

    @Generated(hash = 567826487)
    public Hairdresser(Long id, @NotNull String name, @NotNull Integer experience,
            @NotNull String education, int age, Gender gender, int allowance) {
        this.id = id;
        this.name = name;
        this.experience = experience;
        this.education = education;
        this.age = age;
        this.gender = gender;
        this.allowance = allowance;
    }

    @Generated(hash = 128799709)
    public Hairdresser() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getExperience() {
        return this.experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public String getEducation() {
        return this.education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return this.gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getAllowance() {
        return this.allowance;
    }

    public void setAllowance(int allowance) {
        this.allowance = allowance;
    }

}
