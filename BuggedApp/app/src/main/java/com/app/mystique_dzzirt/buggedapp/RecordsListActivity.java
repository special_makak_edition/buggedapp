package com.app.mystique_dzzirt.buggedapp;

import android.Manifest;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.app.mystique_dzzirt.buggedapp.greenDao.AvailableServices;
import com.app.mystique_dzzirt.buggedapp.greenDao.DaoSession;
import com.app.mystique_dzzirt.buggedapp.greenDao.Hairdresser;
import com.app.mystique_dzzirt.buggedapp.greenDao.Record;
import com.app.mystique_dzzirt.buggedapp.greenDao.RecordDao;
import com.app.mystique_dzzirt.buggedapp.greenDao.Service;
import com.app.mystique_dzzirt.buggedapp.utils.Gender;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Date;
import java.util.List;

import static android.view.View.GONE;

/**
 * Created by Dzzirt on 20.12.2016.
 */

@EActivity(R.layout.record_list_activity)
public class RecordsListActivity extends AppCompatActivity {

    @ViewById(R.id.my_recycler_view)
    public RecyclerView m_recyclerView;

    @ViewById(R.id.add_record_btn)
    public FloatingActionButton m_addRecordBtn;

    @ViewById(R.id.record_list_toolbar)
    public Toolbar m_toolbar;
    private RecordsAdapter m_recordsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //Bug
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CALL_PHONE}, 0);

    }

    @AfterViews
    public void init() {
        setSupportActionBar(m_toolbar);
        DaoSession daoSession = ((App) getApplication()).getDaoSession();
//        createSampleRecord(daoSession);
        RecordDao recordDao = daoSession.getRecordDao();
        List<Record> list = recordDao.queryBuilder().build().list();
        m_recyclerView.setLayoutManager(new LinearLayoutManager(this));
        m_recordsAdapter = new RecordsAdapter(m_recyclerView, list);
        m_recyclerView.setAdapter(m_recordsAdapter);

        m_addRecordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                //Bug
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        RecordCreationActivity_.intent(view.getContext()).start();
                    }
                }, 3000);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        DaoSession daoSession = ((App) getApplication()).getDaoSession();
        RecordDao recordDao = daoSession.getRecordDao();
        m_recordsAdapter.updateData(recordDao.loadAll());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //Bug
        swapButtonVisibility();
    }

    private void swapButtonVisibility() {
        if (m_addRecordBtn != null) {
            m_addRecordBtn.setVisibility(m_addRecordBtn.getVisibility() == GONE ? View.VISIBLE : GONE);
        }
    }

    private void createSampleRecord(DaoSession daoSession) {

        Hairdresser hairdresser = new Hairdresser();
        hairdresser.setAge(12);
        hairdresser.setAllowance(100);
        hairdresser.setEducation("High");
        hairdresser.setExperience(200);
        hairdresser.setGender(Gender.MALE);
        hairdresser.setName("Демидов Дмитрий Олегович");

        Service service = new Service();
        service.setName("Классика");
        service.setDuration(new Date());
        service.setPrice(100);

        daoSession.getHairdresserDao().insert(hairdresser);
        daoSession.getServiceDao().insert(service);

        AvailableServices availableServices = new AvailableServices();
        availableServices.setHairdresser(hairdresser);
        availableServices.setService(service);

        daoSession.getAvailableServicesDao().insert(availableServices);

        Record record = new Record();
        record.setAvailableServices(availableServices);
        record.setDate("21.12.2012");
        daoSession.getRecordDao().insert(record);
    }
}
