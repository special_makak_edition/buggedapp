package com.app.mystique_dzzirt.buggedapp;

/**
 * Created by Admin on 22.12.2016.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.app.mystique_dzzirt.buggedapp.utils.PageFragment;

public class GalleryAdapter extends FragmentPagerAdapter
{
    public GalleryAdapter(FragmentManager mgr) {
        super(mgr);
    }
    @Override
    public int getCount() {
        return(4);
    }
    @Override
    public Fragment getItem(int position)
    {
        return(PageFragment.newInstance(position));
    }
}