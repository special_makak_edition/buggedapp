package com.app.mystique_dzzirt.buggedapp;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.app.mystique_dzzirt.buggedapp.greenDao.AvailableServices;
import com.app.mystique_dzzirt.buggedapp.greenDao.DaoSession;
import com.app.mystique_dzzirt.buggedapp.greenDao.Record;
import com.app.mystique_dzzirt.buggedapp.greenDao.RecordDao;
import com.app.mystique_dzzirt.buggedapp.greenDao.Service;
import com.app.mystique_dzzirt.buggedapp.greenDao.ServiceDao;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Date;

/**
 * Created by Dzzirt on 23.12.2016.
 */

@EActivity(R.layout.service_creation_activity)
public class ServiceCreationActivity extends AppCompatActivity {

    @ViewById(R.id.service_creation_toolbar)
    public Toolbar m_toolbar;

    @ViewById(R.id.service_name)
    public EditText m_serviceName;

    @ViewById(R.id.service_price)
    public EditText m_servicePrice;

    @AfterViews
    public void init() {
        setSupportActionBar(m_toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_changes_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_save : {
                ServiceDao serviceDao = ((App) getApplication()).getDaoSession().getServiceDao();
                Service service = new Service();
                service.setDuration(new Date());
                service.setName(m_serviceName.getText().toString());
                service.setPrice(Integer.valueOf(m_servicePrice.getText().toString()));
                serviceDao.insert(service);
            }
        }
        finish();
        return true;
    }
}
