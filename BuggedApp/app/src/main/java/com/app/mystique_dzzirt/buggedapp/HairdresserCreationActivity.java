package com.app.mystique_dzzirt.buggedapp;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.app.mystique_dzzirt.buggedapp.greenDao.AvailableServices;
import com.app.mystique_dzzirt.buggedapp.greenDao.AvailableServicesDao;
import com.app.mystique_dzzirt.buggedapp.greenDao.DaoSession;
import com.app.mystique_dzzirt.buggedapp.greenDao.Hairdresser;
import com.app.mystique_dzzirt.buggedapp.greenDao.HairdresserDao;
import com.app.mystique_dzzirt.buggedapp.greenDao.Record;
import com.app.mystique_dzzirt.buggedapp.greenDao.Service;
import com.app.mystique_dzzirt.buggedapp.greenDao.ServiceDao;
import com.app.mystique_dzzirt.buggedapp.utils.Gender;
import com.app.mystique_dzzirt.buggedapp.utils.MultiSelectSpinner;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

/**
 * Created by Dzzirt on 22.12.2016.
 */

@EActivity(R.layout.hairdresser_creation_activity)
public class HairdresserCreationActivity extends AppCompatActivity {

    @ViewById(R.id.hairdresser_services)
    public MultiSelectSpinner m_availableServices;

    @ViewById(R.id.hairdresser_allowance)
    public EditText m_allowance;

    @ViewById(R.id.hairdresser_name)
    public EditText m_hairdresserName;

    @ViewById(R.id.hairdresser_creation_toolbar)
    public Toolbar m_toolbar;

    private DaoSession m_daoSession;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @AfterViews
    public void init() {
        setSupportActionBar(m_toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        m_daoSession = ((App) getApplication()).getDaoSession();
        List<Service> list = m_daoSession.getServiceDao().loadAll();
        String[] names = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            names[i] = list.get(i).getName();
        }
        m_availableServices.setItems(names);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_save : {
                HairdresserDao hairdresserDao = m_daoSession.getHairdresserDao();
                ServiceDao serviceDao = m_daoSession.getServiceDao();
                Hairdresser hairdresser = new Hairdresser();
                hairdresser.setAllowance(Integer.valueOf(m_allowance.getText().toString()));
                hairdresser.setName(m_hairdresserName.getText().toString());
                hairdresser.setGender(Gender.MALE);
                hairdresser.setAge(99);
                hairdresser.setExperience(1);
                hairdresser.setEducation("High");
                hairdresserDao.insert(hairdresser);
                AvailableServicesDao availableServicesDao = m_daoSession.getAvailableServicesDao();
                List<Integer> selectedIndicies = m_availableServices.getSelectedIndicies();
                for (Integer index : selectedIndicies) {
                    AvailableServices availableServices = new AvailableServices();
                    availableServices.setHairdresser(hairdresser);
                    Service service = serviceDao.queryBuilder().where(ServiceDao.Properties.Id.eq(index.longValue() + 1)).unique();
                    availableServices.setService(service);
                    availableServicesDao.insert(availableServices);
                }
                break;
            }
        }
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_changes_menu, menu);
        return true;
    }

}
