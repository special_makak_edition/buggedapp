package com.app.mystique_dzzirt.buggedapp;

import android.app.Application;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatDelegate;

import com.app.mystique_dzzirt.buggedapp.greenDao.DaoMaster;
import com.app.mystique_dzzirt.buggedapp.greenDao.DaoSession;

import org.greenrobot.greendao.database.Database;

/**
 * Created by Dzzirt on 20.12.2016.
 */

public class App extends Application {
    /** A flag to show how easily you can switch from standard SQLite to the encrypted SQLCipher. */
    public static final boolean ENCRYPTED = true;

    private DaoSession m_daoSession;

    @Override
    public void onCreate() {
        //TODO запилить админа
        super.onCreate();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "barberShop-db");
        Database db = helper.getWritableDb();
        m_daoSession = new DaoMaster(db).newSession();
    }

    public DaoSession getDaoSession() {
        return m_daoSession;
    }
}