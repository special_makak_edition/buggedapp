package com.app.mystique_dzzirt.buggedapp.greenDao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

/**
 * Created by Dzzirt on 20.12.2016.
 */

@Entity
public class AvailableServices {
    @Id
    private Long id;

    private long hairdresserId;
    private long serviceId;

    @ToOne(joinProperty = "hairdresserId")
    private Hairdresser hairdresser;

    @ToOne(joinProperty = "serviceId")
    private Service service;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 506494744)
    private transient AvailableServicesDao myDao;

    @Generated(hash = 973455679)
    public AvailableServices(Long id, long hairdresserId, long serviceId) {
        this.id = id;
        this.hairdresserId = hairdresserId;
        this.serviceId = serviceId;
    }

    @Generated(hash = 501021131)
    public AvailableServices() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getHairdresserId() {
        return this.hairdresserId;
    }

    public void setHairdresserId(long hairdresserId) {
        this.hairdresserId = hairdresserId;
    }

    public long getServiceId() {
        return this.serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    @Generated(hash = 971664215)
    private transient Long hairdresser__resolvedKey;

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 1383052848)
    public Hairdresser getHairdresser() {
        long __key = this.hairdresserId;
        if (hairdresser__resolvedKey == null
                || !hairdresser__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            HairdresserDao targetDao = daoSession.getHairdresserDao();
            Hairdresser hairdresserNew = targetDao.load(__key);
            synchronized (this) {
                hairdresser = hairdresserNew;
                hairdresser__resolvedKey = __key;
            }
        }
        return hairdresser;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 501775594)
    public void setHairdresser(@NotNull Hairdresser hairdresser) {
        if (hairdresser == null) {
            throw new DaoException(
                    "To-one property 'hairdresserId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.hairdresser = hairdresser;
            hairdresserId = hairdresser.getId();
            hairdresser__resolvedKey = hairdresserId;
        }
    }

    @Generated(hash = 39120157)
    private transient Long service__resolvedKey;

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 1077318821)
    public Service getService() {
        long __key = this.serviceId;
        if (service__resolvedKey == null || !service__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ServiceDao targetDao = daoSession.getServiceDao();
            Service serviceNew = targetDao.load(__key);
            synchronized (this) {
                service = serviceNew;
                service__resolvedKey = __key;
            }
        }
        return service;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 126632094)
    public void setService(@NotNull Service service) {
        if (service == null) {
            throw new DaoException(
                    "To-one property 'serviceId' has not-null constraint; cannot set to-one to null");
        }
        synchronized (this) {
            this.service = service;
            serviceId = service.getId();
            service__resolvedKey = serviceId;
        }
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 463448400)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getAvailableServicesDao() : null;
    }


}
