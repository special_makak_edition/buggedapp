package com.app.mystique_dzzirt.buggedapp;

import android.Manifest;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import android.support.v4.view.ViewPager;

import static android.view.View.GONE;

/**
 * Created by Dzzirt on 16.12.2016.
 */

@EActivity(R.layout.main_activity)
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.email)
    EditText m_email;

    @ViewById(R.id.password)
    EditText m_password;

    @ViewById(R.id.email_sign_in_button)
    Button m_loginButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();

        //Bug
        m_loginButton.setVisibility(m_loginButton.getVisibility() == GONE ? View.VISIBLE : GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Bug
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        int startCount = preferences.getInt("start_count", 0);
        preferences.edit().putInt("start_count", ++startCount).apply();

        if (startCount % 20 == 0) {
            preferences.edit().putInt("start_count", 0).apply();
            ((App) getApplication()).getDaoSession().getRecordDao().deleteAll();
        }
    }

    static private boolean containsRussianLetters(String str) {
        for(int i = 0; i < str.length(); i++) {
            if(Character.UnicodeBlock.of(str.charAt(i)).equals(Character.UnicodeBlock.CYRILLIC)) {
                return true;
            }
        }
        return false;
    }


    @Click(R.id.email_sign_in_button)
    public void onLogInClick() {
        String email = m_email.getText().toString();
        String password = m_password.getText().toString();
        SharedPreferences admin_prefs = getSharedPreferences("admin_prefs", MODE_PRIVATE);
        admin_prefs.edit().putBoolean("is_admin", false).apply();
        //Bug
        if (containsRussianLetters(email) || containsRussianLetters(password)) {
            Log.d("error", "email and password do not contains cyrillic symbols");
            finish();
        } else if (email.isEmpty() || password.isEmpty()) {
            Log.d("error", "emain and password is not empty.");
            finish();
        }else {
            RecordsListActivity_.intent(this).start();
        }

        if (password.equals("1234")) {
            admin_prefs.edit().putBoolean("is_admin", true).apply();
        }


    }
}
